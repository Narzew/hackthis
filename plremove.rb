#encoding: utf-8
files = [
"start.rb",
"Config/Computers.ini",
"Config/Systems.ini",
"Config/Missions.ini",
"SysBin/DOS.bin",
"SysBin/Linux.bin",
"SysBin/HelpSystem.bin",
"Systems/DOS/ECHO.rb",
"Systems/DOS/HELP.rb",
"Systems/DOS/SHUTDOWN.rb",
"Systems/Linux/echo.rb",
"Systems/Linux/help.rb",
"Systems/Linux/shutdown.rb"
]
files.each{|x|
	data = lambda{File.open(x,'rb'){|f|return f.read}}.call
	data.force_encoding("UTF-8")
	data  = data.tr("ąćęłńóśżź","acelnoszz")
	File.open(x,'wb'){|w|w.write(data)}
	print "#{x} parsed.\n"
}
