require 'fileutils'

puts "Updating version file.."
version = lambda{File.open('version.txt','rb'){|f|return f.read}}.call.to_i
version+=1
File.open('version.txt','wb'){|w|w.write(version.to_s)}
puts "Updating version log.."
File.open('versionlog.txt','ab'){|w|w.write("#{version} #{Time.now.year} #{Time.now.month} #{Time.now.day} #{Time.now.hour} #{Time.now.min} #{Time.now.sec}\n")}

Dir.chdir("Config")
puts "Compiling configuration files.."
system("ruby compiler.rb")
Dir.chdir("../")
puts "Copying data.."
FileUtils.mv("Config/Computers.ncp", "Data/Computers.ncp")
FileUtils.mv("Config/Systems.ncp", "Data/Systems.ncp")
FileUtils.mv("Config/Missions.ncp", "Data/Missions.ncp")
FileUtils.mv("Config/Software.ncp", "Data/Software.ncp")
FileUtils.mv("Config/Network.ncp", "Data/Network.ncp")
FileUtils.mv("Config/ExpTable.ncp", "Data/ExpTable.ncp")
FileUtils.cp("Project/engine.rb", "Data/engine.bin")
FileUtils.cp("Project/house.rb", "Data/house.bin")
FileUtils.cp("Project/main.rb", "Data/main.bin")
FileUtils.cp("Project/menus.rb", "Data/menus.bin")
FileUtils.cp("Project/missions.rb", "Data/missions.bin")
FileUtils.cp("Project/profiles.rb", "Data/profiles.bin")
FileUtils.cp("Project/shop.rb", "Data/shop.bin")
FileUtils.cp("Project/narzewfs.rb", "Data/narzewfs.bin")
FileUtils.cp("Project/softwarecenter.rb", "Data/softwarecenter.bin")
puts "Making archive.."
system("ruby ixpack.rb m")
#puts "Removing release folder.."
#system("rm -rf Release")
Dir.mkdir("Release") unless Dir.exist?("Release")
puts "Making release.."
FileUtils.cp("Main.rb", "Release/Main.rb")
FileUtils.cp("Data.ncp", "Release/Data.ncp")
Dir.mkdir("Release/Dict") unless Dir.exist?("Release/Dict")
#FileUtils.cp("Dict/english.dict", "Release/Dict/english.dict")
FileUtils.cp("version.txt", "Release/version.txt")
FileUtils.cp("versionlog.txt", "Release/versionlog.txt")
File.delete("Data.ncp")
Dir.delete("Profiles") if Dir.exist?("Profiles")
print "Archiving release.."
system("tar -czf Old/HackThis_p#{version}.tar.gz Release Config Data Software Systems SysBin Network Project")
puts "Done."
