if $soft_args[0].size > 8
	print "MKDIR: NAME MUST BE 8 BYTE OR LESS\n"
elsif $soft_args[0].upcase != $soft_args[0]
	print "MKDIR: NAME MUST BE WRITTEN IN UPCASE\n"
else
	HackThis.sleep_proc(100)
	nfs_mkdir($soft_args[0])
	nfs_save($nfs_archivename)
	if $available_missions.include?(10)
		HackThis.complete_mission(10)
	end
end
