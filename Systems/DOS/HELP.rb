print "Microsoft DOS v 6.00 Help\n\n"
print "Available commands:\n"
#print "CD - enter a directory (global)\n"
print "CHDIR - enter a directory (relative)\n"
print "CLS - clears screen\n"
#print "COPY - copy a file\n"
#print "DEL - delete a file\n"
print "DIR - show files in directory\n"
print "ECHO - show a text\n"
print "HELP - display this helpfile\n"
print "MKDIR - create a directory\n"
#print "MOVE - synonym for RENAME\n"
#print "RENAME - rename or move a file\n"
print "RMDIR - remove a directory\n"
print "SHUTDOWN - shutdown system\n"
#print "TYPE - show file contents\n"
#print "XCOPY - copy a directory\n"
#print "PROGNAME - run a program\n"
print "\n"*4
if $available_missions.include?(5)
	HackThis.complete_mission(5)
end
