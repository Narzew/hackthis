module CryptoCat
	def self.encrypt(x,k)
		s = ""
		k = k.to_i
		srand((k*7)%0xFFFFFF)
		x.each_byte{|y|
			s << ((y+rand(5))%256).chr
		}
		return s
	rescue
		print "Nie udalo sie zaszyfrowac wiadomosci!\n"
	end
	def self.decrypt(x,k)
		s = ""
		k = k.to_i
		srand((k*7)%0xFFFFFF)
		x.each_byte{|y|
			s << ((y-rand(5))%256).chr
		}
		return s
	rescue
		print "Nie udalo sie odszyfrowac wiadomosci!\n"
	end
	def self.start
		HackThis.sleep_proc(125)
		print "CryptoCat v 1.00\n"
		print "0 - zaszyfruj\n"
		print "1 - odszyfruj\n"
		mode = HackThis.choice(0,1)
		case mode
		when 0
			print "Podaj tekst do zaszyfrowania.\n"
			txt = $stdin.gets.chomp!
			print "Podaj klucz. Klucz musi byc liczba.\n"
			key = $stdin.gets.chomp!.to_i
			print "Szyfrowanie...\n"
			result = CryptoCat.encrypt(txt,key)
			HackThis.sleep_proc(400,30,".")
			print "Zaszyfrowana wiadomosc:\n#{result}\n"
			print "Dziekujemy za uzycie CryptoCata.\n"
		when 1
			print "Podaj zaszyfrowana wiadomosc.\n"
			txt = $stdin.gets.chomp!
			print "Podaj klucz. Klucz musi byc liczba.\n"
			key = $stdin.gets.chomp!.to_i
			print "Deszyfrowanie...\n"
			result = CryptoCat.decrypt(txt,key)
			HackThis.sleep_proc(400,30,".")
			print "Odszyfrowana wiadomosc:\n#{result}\n"
			print "Dziekujemy za uzycie CryptoCata.\n"
		end
	end
end
CryptoCat.start	
