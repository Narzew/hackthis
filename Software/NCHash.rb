module NCHash
	def self.generate_hash(x)
		return Digest::MD5.hexdigest(Digest::SHA1.hexdigest(x).crypt("a1239cs")).crypt("1239af$").to_s
	end
	def self.start
		print "NCHash v 1.00\n"
		HackThis.sleep_proc(30)
		print "Podaj tekst do zahashowania.\n"
		txt = $stdin.gets.chomp!
		print "Hashowanie...\n"
		HackThis.sleep_proc(60)
		hash = NCHash.generate_hash(txt)
		print "Wygenerowany hash: #{hash}\n"
	end
end
NCHash.start