module HackThis

	def self.menu_software_center
		HackThis.clear_screen
		sel
		print "Dostepne oprogramowanie:\n"
		count = 0
		choice_soft = []
		tmp_soft = $software.dup
		a_tmp = tmp_soft.shift
		tmp_soft.each{|x|
			if x[3].to_i != -1 && x[3].to_i <= $globals["level"] && !($my_software.include?(x[0].to_i))
				count += 1
				choice_soft << x[0].to_i
				print "#{count} - #{x[1]}\n"
			end
		}
		lastcount = count+1
		print "#{lastcount} - Powrot\n"
		sel
		choice = HackThis.choice(1,lastcount)
		selsoftid = choice_soft[choice-1]
		if choice == lastcount
			HackThis.main_menu
		else
			HackThis.show_software_info(selsoftid)
			HackThis.buy_software(selsoftid)
		end
	end
	
	def self.get_software_systemlist(x)
		available_systems = $software[x.to_i][5].split(",")
		available_systems.map!{|x| x.to_i }
		return available_systems
	end
	
	def self.print_software_systemlist(x)
		available_systems = $software[x][5].split(",")
		available_systems.map!{|x| x.to_i }
		s = ""
		available_systems.each{|y|
			next if y == "" || y == nil || y == []
			s << "#{$systems[y][1]}, "
		}
		s[-1] = ""
		return s
	end
	
	def self.show_software_info(x)
		HackThis.clear_screen
		sel
		print "Nazwa: #{$software[x][1]}\n"
		print "Opis: #{$software[x][2].gsub("$LN","\n")}\n"
		print "Wymagany poziom: #{$software[x][3]}\n"
		print "Cena: #{$software[x][4]} zl\n"
		print "Zgodne systemy: #{HackThis.print_software_systemlist(x)}\n"
		print "Wymagane miejsce na dysku: #{$software[x][6]} KB\n"
		print "Wymagana pamiec RAM: #{$software[x][7]} MB\n"
		sel
	end
	
	def self.buy_software(x)
		if $my_software.include?(x)
			sel
			print "Posiadasz juz ten program.\n"
			sel
			HackThis.menu_shop
		end
		sel
		print "Posiadasz #{$globals["cash"]} zl\n"
		sel
		if $software[x][4].to_i <= $globals["cash"]
			sel
			print "Czy chcesz kupic ten program? [y/n]\n"
			sel
			choice = $stdin.gets.chomp!
			if choice == "y" || choice == "yes" || choice == "t" || choice == "tak"
				$globals["cash"] -= $software[x][4].to_i
				$my_software << x.to_i
				sel
				print "Kupiono program: #{$software[x][1]}\n"
				sel
				$stdin.gets
				HackThis.menu_software_center
			end
		else
			sel
			print "Nie stac cie na zakup tego programu.\n"
			sel
			$stdin.gets
			HackThis.menu_software_center
		end
		HackThis.menu_shop
	end

end
