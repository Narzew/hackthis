module HackThis

	def self.add_mission(x)
		HackThis.clear_screen
		$available_missions << x unless $available_missions.include?(x)
		sel
		print "Nowa misja!\n"
		HackThis.mission_info(x)
		$stdin.gets
	end

	def self.complete_mission(x)
		if $available_missions.include?(x)
			$available_missions.delete(x)
			$completed_missions << x
			HackThis.mission_completion_info(x)
			$globals["cash"] += $missions[x][3].to_i
			HackThis.add_exp($missions[x][4].to_i)
			$stdin.gets
			if x == $missions_size
				$extra["addmission"] = x+1
				sel
				print "Uwaga!!!\n"
				print "Ukonczyles ostatnia dostepna misje w tej wersji gry.\n"
				print "Gdy zostana dodana nowa misja, dostaniesz ja po wczytaniu profilu.\n"
				sel
			else
				HackThis.add_mission(x+1)
			end
		else
			lambda{}.call
		end
	end
	
	def self.check_mission_state
		HackThis.complete_mission(1) if $my_computers.include?(1)
		HackThis.complete_mission(2) if $my_systems.include?(1)
		HackThis.complete_mission(8) if $my_computers.include?(2)
	end
	
	def self.mission_completion_info(x)
		sel
		print "Misja ukonczona!!!\n"
		print "Misja #{$missions[x][1]} zostala ukonczona!\n"
		ri = $missions[x][5].gsub("\n","").gsub("$LN","\n").gsub("$PLAYERNAME",$globals["name"])
		print "#{ri}\n"
		print "Nagroda:\n"
		print "#{$missions[x][3]} zl\n"
		print "#{$missions[x][4]} EXP\n"
		sel
	end

	def self.mission_info(x)
		sel
		print "#{$missions[x][1]}\n"
		ri = $missions[x][2].gsub('\n',"").gsub("$LN","\n").gsub("$PLAYERNAME",$globals["name"])
		print "#{ri}\n"
		print "Nagroda:\n"
		print "#{$missions[x][3]} zl\n"
		print "#{$missions[x][4]} EXP\n"
		sel
	end

	def self.show_missions
		sel
		print "Dostepne misje:\n"
		count = 0
		$available_missions.each{|x|
			count += 1
			print "#{count} - #{$missions[x][1]}\n"
		}
		lastcount = count+1
		print "#{lastcount} - Powrot\n"
		sel
		choice = HackThis.choice(1,lastcount).to_i
		if choice == lastcount
			HackThis.main_menu
		else
			HackThis.mission_info($available_missions[choice-1])
			$stdin.gets
			HackThis.menu_missions
		end
	end

	def self.show_completed_missions
		if $completed_missions.size == 0
			sel
			print "Brak ukonczonych misji!\n"
			sel
		end
		sel
		print "Ukonczone misje:\n"
		count = 0
		$completed_missions.each{|x|
			count += 1
			print "#{count} - #{$missions[x][1]}\n"
		}
		lastcount = count+1
		print "#{lastcount} - Powrot\n"
		sel
		choice = HackThis.choice(1,lastcount).to_i
		if choice == lastcount
			HackThis.main_menu
		else
			HackThis.mission_info($completed_missions[choice-1])
			$stdin.gets
			HackThis.menu_missions
		end
	end
	
end
