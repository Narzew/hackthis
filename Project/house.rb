module HackThis

	def self.computer_boot
		sel
		if $globals["computer"] != nil && $globals["computer"] != 0
			print "Uruchamianie komputera: #{$computers[$globals["computer"]][1]}\n"
		else
			print "Nie posiadasz komputera!\n"
			sel
			$stdin.gets
			HackThis.menu_house
		end
		system = $computer_systems[$globals["computer"]].to_i
		if $computer_systems[$globals["computer"]] != nil && $computer_systems[$globals["computer"]] != 0
			print "Uruchamianie systemu: #{$systems[$computer_systems[$globals["computer"]]][1]}\n"
		else
			print "Nie posiadasz systemu operacyjnego!\n"
			sel
			$stdin.gets
			HackThis.menu_house
		end
		sel
		if $available_missions.include?(4)
			HackThis.complete_mission(4)
		end
		name = $globals["name"]
		$nfs_archivename = "Profiles/#{name}.nfs"
		$nfs_parent = 0
		NarzewFS.make($nfs_archivename) unless File.exist?($nfs_archivename)
		NarzewFS.read($nfs_archivename)
		if $implemented_systems.include?($systems[system][0].to_i)
			eval(HackThis.load_file("SysBin/#{$systems[system][7]}"))
		else
			sel
			print "Nie zaimplementowano jeszcze systemu #{$systems[system][1]}\n"
			sel
			$stdin.gets
			HackThis.menu_house
		end
		HackThis.menu_house
	end
	
	def self.select_computer
		if $my_computers.size == 0
			sel
			print "Nie posiadasz zadnego komputera!\n"
			sel
			$stdin.gets
			HackThis.menu_house
		end
		count = 0
		sel
		$my_computers.each{|x|
			count += 1
			print "#{count} - #{$computers[x][1]}\n"
		}
		sel
		choice = HackThis.choice(1,count)
		$globals["computer"] = $my_computers[choice.to_i-1]
		sel
		print "Wybrano komputer: #{$computers[$globals["computer"]][1]}\n"
		sel
		$stdin.gets
		if $available_missions.include?(9)
			HackThis.complete_mission(9)
		end
		HackThis.menu_house
	end
	
	def self.install_system
		if $globals["computer"] == nil || $globals["computer"] == 0 || $globals["computer"] == "0"
			sel
			print "Nie posiadasz komputera!\n"
			sel
			$stdin.gets
			HackThis.menu_house
		end
		if $computer_systems[$globals["computer"]] == nil || $computer_systems[$globals["computer"]] == 0
			lambda{}.call
		else
			sel
			print "Ten komputer posiada już system operacyjny.\n"
			sel
			$stdin.gets
			HackThis.menu_house
		end
		if $my_systems.size == 0
			sel
			print "Nie posiadasz zadnego systemu operacyjnego!\n"
			sel
			$stdin.gets
			HackThis.menu_house
		end
		count = 0
		tmp_systems = []
		sel
		$my_systems.each{|x|
			count += 1
			print "#{count} - #{$systems[x][1]}\n"
			tmp_systems << $systems[x][0].to_i
		}
		sel
		selectedos = $systems[tmp_systems[HackThis.choice(1,count)-1]]
		HackThis.show_system_info(selectedos[0].to_i)
		if $computers[$globals["computer"]][6].to_i < selectedos[5].to_i || $computers[$globals["computer"]][5].to_i < selectedos[6].to_i
			sel
			print "Twoj komputer nie splenia minimalnych wymagan systemu #{selectedos[1]}\n"
			sel
			$stdin.gets
			HackThis.menu_house
		end
		print "Czy napewno chcesz zainstalować ten system? [y/n]\n"
		choice = $stdin.gets.chomp!
		if choice == "y" || choice == "yes" || choice == "t" || choice == "tak"
			$computer_systems[$globals["computer"]] = selectedos[0].to_i
			$free_hdd[$globals["computer"]] -= selectedos[6].to_i
			$free_ram[$globals["computer"]] -= selectedos[5].to_i
			$my_systems.delete(selectedos[0].to_i)
			sel
			print "Instalowanie systemu...\n"
			print "Prosze czekac...\n"
			sel
			HackThis.sleep_proc($systems[selectedos[0].to_i][8].to_i,30,"=")
			sel
			print "System operacyjny #{$systems[selectedos[0].to_i][1]} został zainstalowany.\n"
			sel
			$stdin.gets
		else
			sel
			print "Instalacja systemu #{$systems[selectedos][1]} nieudana.\n"
			sel
			$stdin.gets
			HackThis.menu_house
		end
		HackThis.menu_house
	end
	
	def self.computer_info
		if $globals["computer"] == nil || $globals["computer"] == 0
			sel
			print "Nie posiadasz komputera!\n"
			sel
			$stdin.gets
			HackThis.menu_house
		end
		HackThis.my_computer_info
		HackThis.menu_house
	end
	
	def self.my_computer_info
		HackThis.clear_screen
		sel
		x = $computers[$globals["computer"]][0].to_i
		print "Nazwa: #{$computers[x][1]}\n"
		print "Procesor: #{$computers[x][4]} Mhz\n"
		print "RAM: "+$free_ram[x].to_s+"/#{$computers[x][5]} MB\n"
		print "Dysk: "+$free_hdd[x].to_s+"/#{$computers[x][6]} KB\n"
		if $computers[x][7].to_i == 1
			print "Komputer posiada obsluge sieci.\n"
			print "Maksymalna szybkosc lacza: #{$computers[x][8]} KB/s\n"
		else
			print "Komputer nie posiada obslugi sieci.\n"
		end
		print "Zainstalowany system: "+$systems[$computer_systems[x]][1].to_s+"\n"
		sel
		$stdin.gets
		HackThis.menu_house
	end
	
	def self.install_software
		if $computer_systems[$globals["computer"]] == nil || $computer_systems[$globals["computer"]] == 0
			sel
			print "Ten komputer nie posiada systemu operacyjnego!\n"
			sel
			$stdin.gets
			HackThis.menu_house
		end
		if $my_software.size == 0
			sel
			print "Nie posiadasz zadnego programu!\n"
			sel
			$stdin.gets
			HackThis.menu_house
		end
		count = 0
		tmp_software = []
		sel
		$my_software.each{|x|
			count += 1
			print "#{count} - #{$software[x][1]}\n"
			tmp_software << $software[x][0].to_i
		}
		sel
		selectedsoft = $software[tmp_software[HackThis.choice(1,count)-1]]
		if $computer_software[$globals["computer"]] == nil
			$computer_software[$globals["computer"]] = []
		end
		unless HackThis.get_software_systemlist(selectedsoft[0]).include?($globals["system"].to_i)
			sel
			print "Nie mozna zainstalowac tego programu na tym systemie operacyjnym!\n"
			sel
			$stdin.gets
			HackThis.menu_house
		end
		if $computer_software[$globals["computer"]].include?(selectedsoft[0])
			sel
			print "Ten program jest juz zainstalowany!\n"
			sel
			$stdin.gets
			HackThis.menu_house
		end
		HackThis.show_software_info(selectedsoft[0].to_i)
		if $free_ram[$globals["computer"]].to_i < selectedsoft[7].to_i || $free_hdd[$globals["computer"]].to_i < selectedsoft[6].to_i
			sel
			print "Twoj komputer nie splenia minimalnych wymagan programu #{selectedsoft[1]}\n"
			sel
			$stdin.gets
			HackThis.menu_house
		end
		print "Czy napewno chcesz zainstalowac ten program? [y/n]\n"
		choice = $stdin.gets.chomp!
		if choice == "y" || choice == "yes" || choice == "t" || choice == "tak"
			sel
			print "Instalowanie programu...\n"
			print "Prosze czekac...\n"
			sel
			HackThis.sleep_proc($software[selectedsoft[0].to_i][8].to_i,30,"=")
			$my_software.delete(selectedsoft[0].to_i)
			$computer_software[$globals["computer"]] << selectedsoft[0].to_i
			$free_hdd[$globals["computer"]] -= selectedsoft[6].to_i
			$free_ram[$globals["computer"]] -= selectedsoft[7].to_i
			sel
			print "Program #{$software[selectedsoft[0].to_i][1]} został zainstalowany.\n"
			sel
			$stdin.gets
		else
			sel
			print "Instalacja programu #{$software[selectedsoft][1]} nieudana.\n"
			sel
			$stdin.gets
			HackThis.menu_house
		end
		HackThis.menu_house
	end
	
end
