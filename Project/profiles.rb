module HackThis

	def self.profile_create
		HackThis.make_game_variables
		print "Jak masz na imie? "
		name = $stdin.gets.chomp!
		$globals["name"] = name
		$globals["cash"] = 800
		$globals["level"] = 1
		$globals["exp"] = 0
		$globals["computer"] = 0
		$globals["system"] = 0
		HackThis.add_mission(1)
		HackThis.save_game
		HackThis.main_menu
	end
	
	def self.save_game
		$result = [$globals,$variables,$available_missions,$completed_missions,$temp_missions,$my_software,
		$my_computers,$my_systems,$computer_systems,$computer_software,$free_hdd,$free_ram,$extra]
		File.open("Profiles/"+$globals["name"]+".ncp",'wb'){|w|w.write(Zlib::Deflate.deflate(Marshal.dump($result),9))}
		sel
		print "Gra zapisana.\n"
		sel
		if $available_missions.include?(3)
			HackThis.complete_mission(3)
		end
		HackThis.main_menu
	end
	
	def self.load_game_menu
		savenames = []
		Find.find("Profiles").each{|x|
			ext = x.split(".")[-1]
			next if ext != "ncp"
			next if x == "."
			next if x == ".."
			next if x == "Thumbs.db"
			savenames << x.gsub("Profiles/","").gsub(".ncp","")
		}
		if savenames.size != 0
			count = 1
			sel
			print "Wybierz profil:\n"
			savenames.each{|x|
				print "#{count} - #{x}\n"
				count += 1
			}
			sel
			choice = HackThis.choice(1,count)
			HackThis.load_game("Profiles/#{savenames[choice-1]}.ncp")
		else
			sel
			print "Brak zapisanych gier.\n"
			sel
			HackThis.start_menu
		end
	end
	
	def self.load_game(x)
		HackThis.make_game_variables
		data = Marshal.load(lambda{File.open(x,'rb'){|f|return Zlib::Inflate.inflate(f.read)}}.call)
		$globals = data[0]
		$variables = data[1]
		$available_missions = data[2]
		$completed_missions = data[3]
		$temp_missions = data[4]
		$my_software = data[5]
		$my_computers = data[6]
		$my_systems = data[7]
		$computer_systems = data[8]
		$computer_software = data[9]
		$free_hdd = data[10]
		$free_ram = data[11]
		$extra = data[12]
		sel
		print "Gra wczytana.\n"
		sel
		HackThis.check_level
		HackThis.main_menu
	end
	
end
