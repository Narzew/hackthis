module HackThis
	
	def self.start_menu
		sel
		print "1 - Nowa gra\n"
		print "2 - Wczytaj gre\n"
		print "3 - Aktualizuj gre\n"
		print "4 - Koniec\n"
		sel
		choice = HackThis.choice(1,4)
		case choice.to_i
		when 1 then HackThis.profile_create
		when 2 then HackThis.load_game_menu
		when 3 then HackThis.patch_game
		when 4 then exit
		end
	end

	def self.main_menu
		HackThis.clear_screen
		HackThis.check_mission_state
		sel
		print "1 - Moj dom\n"
		print "2 - Sklep komputerowy\n"
		print "3 - Software Center\n"
		print "4 - Misje\n"
		print "5 - Pomoc\n"
		print "6 - Zapisz gre\n"
		print "7 - Wyjscie\n"
		print "8 - Debug Terminal\n" if $debug_mode == true
		sel
		if $debug_mode == true
			choice = HackThis.choice(1,8)
		else
			choice = HackThis.choice(1,7)
		end
		case choice
		when 1 then HackThis.menu_house
		when 2 then HackThis.menu_shop
		when 3 then HackThis.menu_software_center
		when 4 then HackThis.menu_missions
		when 5 then HackThis.menu_help
		when 6 then HackThis.save_game
		when 7 then HackThis.start_menu
		when 8 then HackThis.debug_console
		end
	end
	
	def self.menu_missions
		HackThis.clear_screen
		sel
		print "1 - Aktywne misje\n"
		print "2 - Ukonczone misje\n"
		print "3 - Powrot\n"
		sel
		choice = HackThis.choice(1,3)
		case choice
		when 1 then HackThis.show_missions
		when 2 then HackThis.show_completed_missions
		when 3 then HackThis.main_menu
		end
	end
	
	def self.menu_help
		HackThis.clear_screen
		eval(HackThis.load_file("SysBin/HelpSystem.bin"))
		HackThis.main_menu
	end
	
	def self.menu_house
		HackThis.clear_screen
		sel
		print "1 - Uruchom komputer\n"
		print "2 - Wybierz komputer\n"
		print "3 - Zainstaluj system operacyjny\n"
		print "4 - Zainstaluj oprogramowanie\n"
		print "5 - Informacje o komputerze\n"
		print "6 - Powrot\n"
		sel
		choice = HackThis.choice(1,6)
		case choice
		when 1 then HackThis.computer_boot
		when 2 then HackThis.select_computer
		when 3 then HackThis.install_system
		when 4 then HackThis.install_software
		when 5 then HackThis.computer_info
		when 6 then HackThis.main_menu
		end
	end
	
end
