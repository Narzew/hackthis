=begin
**Narzew Filesystem
**Copyright (C) 23.01.2013 Narzew
**All rights reserved.
**Using without permission is illegal.
=end

=begin
** Struktura:
$files[a,a,a..]
gdzie a[id] = [folder_id,name,orig_name,size,orig_size,attrs,special,content]

$directories[id] = [a,a,a..]
gdzie a = [parent_id,name,orig_name,attrs,special]

=end

module NarzewFS

	def self.make(x)
		$files = {}
		$directories = {}
		$filid = 0
		$dirid = 0
		$result=["NFS1",$files,$directories,$filid,$dirid]
		File.open(x,'wb'){|w|w.write(Zlib::Deflate.deflate(Marshal.dump($result)))}
	end
	
	def self.save(x)
		$result=["NFS1",$files,$directories,$filid,$dirid]
		File.open(x,'wb'){|w|w.write(Zlib::Deflate.deflate(Marshal.dump($result)))}
	end
	
	def self.read(x)
		data = lambda{File.open(x,'rb'){|f|return Marshal.load(Zlib::Inflate.inflate(f.read))}}.call
		print("NarzewFS Corrupted Archive") if data[0] != "NFS1"
		$files = data[1]
		$directories = data[2]
		$filid = data[3]
		$dirid = data[4]
	rescue
		print("NarzewFS Corrupted Archive\n")
	end
	
	def self.add_directory(name, parent=0, attrs="", special=0)
		if name == ""
			print "Podaj nazwe pliku!\n"
		elsif list_directory(parent).include?(name)
			print "Katalog juz instnieje!\n"
		else
			$dirid += 1
			$directories[$dirid] = [parent,name,name,attrs,special]
		end
	rescue
		print("Cannot add directory.\n")
	end
	
	#gdzie a[id] = [folder_id,name,orig_name,size,orig_size,attrs,special,content]
	
	def add_file(name,folder=0,size=0,attrs="",special=0,content="")
		if folder == 0
			folder = $nfs_parent
		end
		if size == 0
			size == content.size
		end
		$filid += 1
		$files[$filid] = [folder,name,name,size,size,attrs,special,content]
	rescue
		print("Cannot add file")
	end
	
	def self.remove_file(id)
		$files[id] = ""
	rescue
		print("Cannot remove file.\n")
	end
	
	def self.remove_directory(id)
		$directories[id] = ""
	rescue
		print "Cannot remove directory.\n"
	end
	
	def self.rename_directory(id,name)
		$directories[id][1] = name
	rescue
		print "Cannot rename directory\n"
	end
	
	def self.mv_directory(name,name2)
		NarzewFS.rename_directory(NarzewFS.get_dir_id(name,parent),name2)
	rescue
		print "Cannot rename directory.\n"
	end
	
	def self.delete_directory(name, parent)
		id = NarzewFS.get_dir_id rescue id == 0
		if id == 0
			print "Cannot remove directory #{name}\n"
		else
			NarzewFS.remove_directory(NarzewFS.get_dir_id(name,parent))
		end
	rescue
		print "Cannot remove directory.\n"
	end
	
	def self.list_directory(pid)
		dirs = []
		$directories.each{|x,y|
			next if x == "" || x == nil
			dirs << y[1] if y[0] == pid
		}
		return dirs
	rescue
		print "Cannot list directories.\n"
	end
	
	def self.ls_directory(pid=0)
		NarzewFS.list_directory(pid).each{|x|
			print "#{x}\n"
		}
	rescue
		print "Cannot list directories.\n"
	end
	
	def self.get_dir_id(name, parent=0)
		$directories.each{|x,y|
			next if x == ""
			return x if y[1] == name && y[0] == parent
		}
	rescue
		print "Cannot get dir ID.\n"
	end
	
	def self.get_cur_dir
		if $nfs_parent == 0
			return "/"
		else
			return $directories[$nfs_parent][2]
		end
	rescue
		print "Cannot get current directory.\n"
	end
	
	def self.get_pid
		if $nfs_parent == 0
			return 0
		else
			return $directories[$nfs_parent][0]
		end
	rescue
		print "Cannot get parent id.\n"
	end
	
end

def nfs_make(x)
	NarzewFS.make(x)
rescue
	print "Cannot make NarzewFS archive.\n"
end

def nfs_save(x)
	NarzewFS.save(x)
rescue
	print "Cannot save NarzewFS archive.\n"
end

def nfs_read(x)
	NarzewFS.read(x)
rescue
	print "NarzewFS archive corrupted.\n"
end

def nfs_ls
	NarzewFS.ls_directory($nfs_parent)
rescue
	print "Cannot list directories.\n"
end

def nfs_mkdir(x)
	NarzewFS.add_directory(x, $nfs_parent)
rescue
	print "Directory creation failed.\n"
end

def nfs_rmdir(x)
	NarzewFS.delete_directory(x, $nfs_parent)
rescue
	print "Cannot remove NarzewFS archive.\n"
end

def nfs_cd(x)
	if x == "../"
		if $nfs_parent == 0
			print "You are in the / directory\n"
		else
			$nfs_parent = $directories[$nfs_parent][0]
		end
	else
		$nfs_parent = NarzewFS.get_dir_id(x,$nfs_parent)
	end
rescue
	print "Cannot cd to directory #{x}\n"
end

def nfs_cdup
	if $nfs_parent == 0
		print "You are in the / directory\n"
	else
		$nfs_parent = $directories[$nfs_parent][0]
	end
rescue
	print "Cannot cd up.\n"
end

def nfs_mvdir(x,y)
	NarzewFS.mv_directory(x,y)
end
