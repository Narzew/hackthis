module HackThis

	def self.menu_shop
		HackThis.check_mission_state
		HackThis.clear_screen
		sel
		print "1 - Komputery\n"
		print "2 - Systemy operacyjne\n"
		print "3 - Powrot\n"
		sel
		choice = HackThis.choice(1,3)
		case choice
		when 1 then HackThis.shop_computers
		when 2 then HackThis.shop_systems
		when 3 then HackThis.main_menu
		end
	end
	
	def self.shop_computers
		HackThis.print_available_computers
		HackThis.main_menu
	end

	def self.print_available_computers
		HackThis.clear_screen
		sel
		print "Dostepne komputery:\n"
		count = 0
		choice_comp = []
		tmp_comps = $computers.dup
		a_tmp = tmp_comps.shift
		tmp_comps.each{|x|
			if x[2].to_i != -1 && x[2].to_i <= $globals["level"] && !($my_computers.include?(x[0].to_i))
				count += 1
				choice_comp << x[0].to_i
				print "#{count} - #{x[1]}\n"
			end
		}
		lastcount = count+1
		print "#{lastcount} - Powrot\n"
		sel
		choice = HackThis.choice(1,lastcount)
		selcompid = choice_comp[choice-1]
		if choice == lastcount
			HackThis.main_menu
		else
			HackThis.show_computer_info(selcompid)
			HackThis.buy_computer(selcompid)
		end
	end
	
	def self.show_computer_info(x)
		HackThis.clear_screen
		sel
		print "Nazwa: #{$computers[x][1]}\n"
		print "Wymagany poziom: #{$computers[x][2]}\n"
		print "Cena: #{$computers[x][3]} zl\n"
		print "Procesor: #{$computers[x][4]} Mhz\n"
		print "RAM: #{$computers[x][5]} MB\n"
		print "Dysk: #{$computers[x][6]} KB\n"
		if $computers[x][7].to_i == 1
			print "Komputer posiada obsluge sieci.\n"
			print "Maksymalna szybkosc lacza: #{$computers[x][8]} KB/s\n"
		else
			print "Komputer nie posiada obslugi sieci.\n"
		end
		sel
	end
	
	def self.buy_computer(x)
		if $my_computers.include?(x)
			sel
			print "Posiadasz juz ten komputer.\n"
			sel
			HackThis.menu_shop
		end
		sel
		print "Posiadasz #{$globals["cash"]} zl\n"
		sel
		if $computers[x][3].to_i <= $globals["cash"]
			sel
			print "Czy chcesz kupic ten komputer? [y/n]\n"
			sel
			choice = $stdin.gets.chomp!
			if choice == "y" || choice == "yes" || choice == "t" || choice == "tak"
				$globals["cash"] -= $computers[x][3].to_i
				$my_computers << x.to_i
				$free_ram[$computers[x.to_i][0].to_i] = $computers[x][5].to_i
				$free_hdd[$computers[x.to_i][0].to_i] = $computers[x][6].to_i
				$globals["computer"] = x.to_i
				sel
				print "Kupiono komputer: #{$computers[x][1]}\n"
				sel
			end
		else
			sel
			print "Nie stac cie na zakup tego komputera.\n"
			sel
		end
		HackThis.menu_shop
	end

	def self.shop_systems
		HackThis.clear_screen
		sel
		print "Dostepne systemy operacyjne:\n"
		count = 0
		choice_sys = []
		tmp_systems = $systems.dup
		a_tmp = tmp_systems.shift
		tmp_systems.each{|x|
			if x[3].to_i != -1 && x[3].to_i <= $globals["level"] && !($my_systems.include?(x[0].to_i))
				count += 1
				choice_sys << x[0].to_i
				print "#{count} - #{x[1]}\n"
			end
		}
		lastcount = count+1
		print "#{lastcount} - Powrot\n"
		sel
		choice = HackThis.choice(1,lastcount)
		selsysid = choice_sys[choice-1]
		if choice == lastcount
			HackThis.main_menu
		else
			HackThis.show_system_info(selsysid)
			HackThis.buy_system(selsysid)
		end
	end
	
	def self.show_system_info(x)
		HackThis.clear_screen
		sel
		print "Nazwa: #{$systems[x][1]}\n"
		print "Opis: #{$systems[x][2].gsub("$LN","\n")}\n"
		print "Wymagany poziom: #{$systems[x][3]}\n"
		print "Cena: #{$systems[x][4]} zl\n"
		print "Wymagane miejsce na dysku: #{$systems[x][5]} KB\n"
		print "Zuzycie RAMu: #{$systems[x][6]} MB\n"
		sel
	end
	
	def self.buy_system(x)
		if $my_systems.include?(x)
			sel
			print "Posiadasz juz ten system operacyjny.\n"
			sel
			HackThis.menu_shop
		end
		sel
		print "Posiadasz #{$globals["cash"]} zl\n"
		sel
		if $systems[x][4].to_i <= $globals["cash"]
			sel
			print "Czy chcesz kupic ten system operacyjny? [y/n]\n"
			sel
			choice = $stdin.gets.chomp!
			if choice == "y" || choice == "yes" || choice == "t" || choice == "tak"
				$globals["cash"] -= $systems[x][4].to_i
				$my_systems << x.to_i
				$globals["system"] = x.to_i
				sel
				print "Kupiono system operacyjny: #{$systems[x][1]}\n"
				sel
			end
		else
			sel
			print "Nie stac cie na zakup tego systemu operacyjnego.\n"
			sel
		end
		HackThis.menu_shop
	end

end
