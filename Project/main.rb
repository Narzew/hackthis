require 'zlib'
require 'find'
require 'fileutils'
require 'open-uri'

if ARGV[0] != nil && ARGV[0] == "-d"
	$debug_mode = true
else
	$debug_mode = false
end

$project_files = ["engine","menus","profiles", "missions", "house", "shop","narzewfs","softwarecenter"]
$project_files.each{|x|
	eval(HackThis.load_file("Data/#{x}.bin"))
}

module HackThis

	def self.start
		HackThis.show_header
		HackThis.prepare
		HackThis.start_menu
	end
	
end

begin
	HackThis.start
end
