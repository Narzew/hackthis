$implemented_systems = [1,2]

def sel
	print "**********************************************************\n"
end

module HackThis

	def self.prepare
		HackThis.load_operating_system
		HackThis.make_game_variables
		HackThis.load_computers
		HackThis.load_systems
		HackThis.load_missions
		HackThis.load_exptable
		HackThis.load_software
		HackThis.load_network
		Dir.mkdir("Profiles") unless Dir.exist?("Profiles")
	end
	
	def self.make_game_variables
		$globals = {}
		$variables = {}
		$disk = []
		$available_missions = []
		$completed_missions = []
		$temp_missions = []
		$my_software = []
		$my_computers = []
		$my_systems = []
		$computer_systems = {}
		$computer_software = {}
		$free_hdd = {}
		$free_ram = {}
		$extra = {}
	end
	
	def self.debug_console
		loop do
			print "\n# "
			cmd = $stdin.gets.chomp!
			eval(cmd)
		end
	end

	def self.load_operating_system
		if (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
			$os = 0
		else
			$os = 1
		end
	end
	
	def self.clear_screen
		if $os == 0
			system("cls")
		else
			system("clear")
		end
	end
	
	def self.sleep_proc(x,y=nil,z="")
		if y == nil || z == ""
			sleep x.to_f/$computers[$globals["computer"]][4].to_f
		else
			t = (x.to_f/$computers[$globals["computer"]][4].to_f)/y.to_i
			y.to_i.times{|a|
				sleep t
				print z
			}
			print "\n"
		end
	end

	def self.uri_get(x,y)
		open(x){|s|File.open(y,'wb'){|f|f.write(s.read)}}
	end

	def self.patch_game
		print "Patchowanie gry...\n"
		HackThis.uri_get("http://hacktut.org/hackthis/patchfile.txt", "patchfile.txt")
		if File.exist?("patchfile.txt")
			patchdata = File.read("patchfile.txt")
			File.delete("patchfile.txt") if File.exist?("patchfile.txt")
		else
			print "Patchowanie nieudane!\n"
		end
		patchdata.each_line{|line|
			data = line.split("|")
			print "Pobieranie #{data[1]}"
			HackThis.uri_get(data[0],data[1].tr("\n\r\t",""))
		}
		print "\nPatchowanie pomyslne.\nNiektore funkcje moga byc dostepne dopiero po zrestartowaniu gry.\n"
		HackThis.start_menu
	rescue => e
		File.delete("patchfile.txt") if File.exist?("patchfile.txt")
		print "Patchowanie nieudane!\n"
		HackThis.start_menu
	end

	def self.show_header
		HackThis.clear_screen
		sel
		print "**Hack This! Hacking Simulator\n"
		print "**by Narzew\n"
		v = lambda{File.open('version.txt','rb'){|f|return f.read}}.call
		print "**v 0.4 p#{v}\n"
		print "**All rights reserved.\n"
		sel
		print "\n\n\n"
	end
	
	def self.load_computers
		data = HackThis.load_file("Data/Computers.ncp")
		data = Zlib::Inflate.inflate(data)
		$computers = data.split("\x00")
		$computers.map! {|x| x = x.split("\x01") }
		$computers_size = $computers.size-1
	end
	
	def self.load_systems
		data = HackThis.load_file("Data/Systems.ncp")
		data = Zlib::Inflate.inflate(data)
		$systems = data.split("\x00")
		$systems.map! {|x| x = x.split("\x01") }
		$systems_size = $systems.size-1
	end

	def self.load_missions
		data = HackThis.load_file("Data/Missions.ncp")
		data = Zlib::Inflate.inflate(data)
		$missions = data.split("\x00")
		$missions.map! {|x| x = x.split("\x01") }
		$missions_size = $missions.size-1
	end
	
	def self.load_exptable
		data = HackThis.load_file('Data/ExpTable.ncp')
		data = Zlib::Inflate.inflate(data)
		$exptable = data.split("\x00")
		$exptable.map!{|x| x.to_i }
	end
	
	def self.load_software
		data = HackThis.load_file("Data/Software.ncp")
		data = Zlib::Inflate.inflate(data)
		$software = data.split("\x00")
		$software.map! {|x| x = x.split("\x01") }
		$software_size = $software.size-1
	end
	
	def self.load_network
		data = HackThis.load_file("Data/Network.ncp")
		data = Zlib::Inflate.inflate(data)
		$network = data.split("\x00")
		$network.map! {|x| x = x.split("\x01") }
		$network_size = $network.size-1
	end
	
	def self.get_level
		count = -1
		$exptable.each{|y|
			count += 1
			next if $globals["exp"] >= y
			return count
		}
	end
	
	def self.check_level
		lv = HackThis.get_level
		if $globals["level"] < lv
			$globals["level"] = lv
			HackThis.new_level_info
		end
	end
	
	def self.new_level_info
		sel
		print "Nowy poziom!\n"
		print "Awansowales na poziom #{$globals["level"]}!\n"
		sel
	end
	
	def self.add_exp(x)
		$globals["exp"] += x
		HackThis.check_level
	end
	
	def self.choice(min,max)
		min = min.to_i
		max = max.to_i
		loop do
			y = $stdin.gets.chomp!.to_i
			if y < min || y > max
				sel
				print "Zly wybor!\n"
				sel
			else
				return y.to_i
			end
		end
		return y.to_i
	end
	
end
