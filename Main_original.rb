#encoding: utf-8
require 'zlib'
require 'digest/sha1'
$archive_name = "Data.ncp"
$master_key2 = "d927cdf3"
module HackThis
	def self.transform_key(x)
		r = 0
		c = 1
		x.to_s.crypt("#{$master_key2}-#{$master_key}").each_byte{|x|
			r += x.to_i*(c*256)
			c += 1
		}
		r = r%0xFFFFFF
		return r
	end
	def self.decrypt_stream(x,k)
		k = HackThis.transform_key(k)
		srand(HackThis.transform_key(k))
		s = ""
		x.each_byte{|b|
			s << ((b-k-7-rand(999999)+transform_key("#{$master_key}+#{$master_key2}"))%256).chr
			k=(k*6+4)%0xFFFFFF
			srand(k)
		}
		return s
	end
	def self.load_file(fl,arch="Data.ncp")
		data = lambda{|x|File.open(x,'rb'){|f|return f.read}}.call(arch)
		data = Marshal.load(data)
		$master_key = data[1]
		data = data[2]
		name = Digest::SHA1.hexdigest(fl).crypt("sfn9as8fh#{$master_key2}$")
		data.each{|x|
			if x[0] == name
				data = x[1]
				break
			end
		}
		key = Digest::SHA1.hexdigest(fl).crypt("#{$master_key}")
		data = HackThis.decrypt_stream(data,key)
		data = Zlib::Inflate.inflate(data)
		return data
	end
end
eval(HackThis.load_file("Data/main.bin"))
