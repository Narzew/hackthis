sel
print "Witaj w systemie pomocy gry HackThis!\n"
print "Wybierz temat pomocy\n"
sel
print "1 - Podstawy gry\n"
print "2 - Powrot\n"
sel
choice = HackThis.choice(1,2)
case choice
when 1
	sel
	print "1 - O grze\n"
	print "2 - Dom\n"
	print "3 - Sklep komputerowy\n"
	print "4 - Uruchamianie komputera\n"
	print "5 - Polecenia systemu operacyjnego\n"
	print "6 - Misje\n"
	print "7 - Powrot\n"
	sel
	choice = HackThis.choice(1,7)
	case choice
	when 1
		print "HackThis! jest symulatorem hackingu.\nGra ma na celu nauczyc gracza obslugi wiersza polecen DOS'a i Linuxowego terminala, oraz niektorych technik hakerskich. I najwazniejsze! Gracz ma sie poczuc jak prawdziwy haker ;)\n"
		$stdin.gets
		HackThis.menu_help
	when 2
		print "W domu mozesz wlaczyc komputer, zobaczyc informacje o kompie i systemie operacyjnym, mozesz takze zmienic obecny komputer lub system operacyjny.\n"
		$stdin.gets
		HackThis.menu_help
	when 3
		print "W sklepie komputerowym mozesz kupic komputery lub systemy operacyjne. Im wieksza ilosc RAM'u tym mocniejsze programy/systemy jest w stanie uruchomic komputer. Im wiecej miejsca na dysku, tym wiecej programow moze pomiescic komputer. Im szybszy procesor, tym szybciej sa wykonywane operacje.\nRozne systemy operacyjne maja rozne instrukcje oraz rozne zgodne oprogramowanie.\n"
		$stdin.gets
		HackThis.menu_help
	when 4
		print "Po uruchomieniu komputera musisz poczekac na zaladowania sie systemu operacyjnego. Po zaladowaniu systemu zobaczysz linie polecen danego systemu.\n"
		$stdin.gets
		HackThis.menu_help
	when 5
		print "Kazdy system operacyjny ma rozne polecenia. W wiekszosci przypadkow, polecenia mozna uzyc uzywajac polecen HELP,help lub /?\n"
		$stdin.gets
		HackThis.menu_help
	when 6
		print "Za wykonywanie misji mozna zdobyc doswiadczenie i pieniadze. Im trudniejsza misja, tym wieksze wynagrodzenie za nia.\n"
		$stdin.gets
		HackThis.menu_help
	when 7
		HackThis.menu_help
	end
when 2
	HackThis.main_menu
end
