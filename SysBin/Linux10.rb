$linux_commands = ["chdir","clear","echo","help","ls","mkdir","rmdir","shutdown"]
print "Starting kernel..\n"
print "\n"*6
sleep 30/$computers[$globals["computer"]][5].to_f
print "Loading kernel modules..\n"
sleep 60/$computers[$globals["computer"]][5].to_f
50.times{
print "."
sleep 3.5/$computers[$globals["computer"]][5].to_f
}
$system_running = true
print "\n\n\n\n"
while ($system_running == true)
	print "$ "
	command = $stdin.gets.chomp!
	a = command.split(" ")
	$soft_command = a.shift
	$soft_args = a
	soft_com = {}
	if $computer_software[$globals["computer"]] != nil
		$computer_software[$globals["computer"]].each{|x|
			soft_com[$software[x][1]] = $software[x][9]
		}
	end
	if $linux_commands.include?($soft_command)
		sleep 1/$computers[$globals["computer"]][5].to_f
		eval(HackThis.load_file("Systems/Linux10/#{$soft_command}.rb"))
	elsif soft_com.include?($soft_command)
		eval(HackThis.load_file("Software/#{soft_com[$soft_command]}"))
	else
		print "#{$soft_command} : not recognized.\n"
	end
end
