#encoding: utf-8
#$dos_commands = ["CD","CHDIR","CLS","COPY","DEL","DIR","ECHO","HELP","MKDIR","MOVE","RENAME","RMDIR","SHUTDOWN","XCOPY"]
$dos_commands = ["CHDIR","CLS","DIR", "ECHO", "HELP", "MKDIR", "RMDIR", "SHUTDOWN"]
print "Uruchamianie systemu DOS..\n"
print "\n"*6
sleep 10/$computers[$globals["computer"]][5].to_f
print "Loading..\n"
sleep 15/$computers[$globals["computer"]][5].to_f
25.times{
print "="
sleep 2.5/$computers[$globals["computer"]][5].to_f
}
$system_running = true
HackThis.clear_screen
print "\n\n\nDOS Prompt ready.\n"
while ($system_running == true)
	print ">"
	command = $stdin.gets.chomp!
	a = command.split(" ")
	$soft_command = a.shift
	$soft_args = a
	soft_com = {}
	if $computer_software[$globals["computer"]] != nil
		$computer_software[$globals["computer"]].each{|x|
			soft_com[$software[x][1]] = $software[x][9]
		}
	end
	if $dos_commands.include?($soft_command)
		sleep 1/$computers[$globals["computer"]][5].to_f
		eval(HackThis.load_file("Systems/DOS/#{$soft_command}.rb"))
	elsif soft_com.include?($soft_command)
		eval(HackThis.load_file("Software/#{soft_com[$soft_command]}"))
	else
		print "Bad command.\n"
	end
end
