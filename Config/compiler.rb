require 'zlib'
module HackThis

	def self.compile_computers
		$result = []
		$act_id = 0
		data = lambda{File.open('Computers.ini','rb'){|f| return f.read}}.call
		data.each_line{|x|
			next if x[0] == "#" || x[0] == ";" || (x[0] == "/" && x[1] == "/")
			a = x.gsub("\n","").split("=")
			com,arg = a[0],a[1]
			case com
			when "Id"
				$act_id = arg.to_i
				$result[$act_id] = []
				$result[$act_id][0] = $act_id
			when "Name"
				$result[$act_id][1] = arg.to_s
			when "Level"
				$result[$act_id][2] = arg.to_i
			when "Cost"
				$result[$act_id][3] = arg.to_i
			when "Proc"
				$result[$act_id][4] = arg.to_i
			when "RAM"
				$result[$act_id][5] = arg.to_i
			when "HDD"
				$result[$act_id][6] = arg.to_i
			when "NetworkSupport"
				$result[$act_id][7] = arg.to_i
			when "NetworkCard"
				$result[$act_id][8] = arg.to_f
			else
				raise("Computers compilation failed on #{com}=#{arg}")
			end
		}
		$result2 = ""
		$result.each{|x|
			Range.new(0,8).each{|y|
				$result2 << "#{x[y]}\x01" rescue lambda{}.call
			}
			$result2 << "\x00"
		}
		File.open("Computers.ncp","wb"){|f|f.write(Zlib::Deflate.deflate($result2,9))}
	end
	
	def self.compile_systems
		$result = []
		$act_id = 0
		data = lambda{File.open('Systems.ini','rb'){|f| return f.read}}.call
		data.each_line{|x|
			next if x[0] == "#" || x[0] == ";" || (x[0] == "/" && x[1] == "/")
			a = x.gsub("\n","").split("=")
			com,arg = a[0],a[1]
			case com
			when "Id"
				$act_id = arg.to_i
				$result[$act_id] = []
				$result[$act_id][0] = $act_id
			when "Name"
				$result[$act_id][1] = arg.to_s
			when "Description"
				$result[$act_id][2] = arg.to_s
			when "Level"
				$result[$act_id][3] = arg.to_i
			when "Cost"
				$result[$act_id][4] = arg.to_i
			when "HDD"
				$result[$act_id][5] = arg.to_i
			when "RAM"
				$result[$act_id][6] = arg.to_i
			when "SystemFile"
				$result[$act_id][7] = arg.to_s
			when "InstallTime"
				$result[$act_id][8] = arg.to_i
			else
				raise("Systems compilation failed on #{com}=#{arg}")
			end
		}
		$result2 = ""
		$result.each{|x|
			Range.new(0,8).each{|y|
				$result2 << "#{x[y]}\x01" rescue lambda{}.call
			}
			$result2 << "\x00"
		}
		File.open("Systems.ncp","wb"){|f|f.write(Zlib::Deflate.deflate($result2,9))}
	end
	
	def self.compile_missions
		$result = []
		$act_id = 0
		data = lambda{File.open('Missions.ini','rb'){|f| return f.read}}.call
		data.each_line{|x|
			next if x[0] == "#" || x[0] == ";" || (x[0] == "/" && x[1] == "/")
			a = x.gsub("\n","").split("=")
			com,arg = a[0],a[1]
			case com
			when "Id"
				$act_id = arg.to_i
				$result[$act_id] = []
				$result[$act_id][0] = $act_id
			when "Name"
				$result[$act_id][1] = arg.to_s
			when "Description"
				$result[$act_id][2] = arg.to_s
			when "Cash"
				$result[$act_id][3] = arg.to_i
			when "Exp"
				$result[$act_id][4] = arg.to_i
			when "RewardInfo"
				$result[$act_id][5] = arg.to_s
			else
				raise("Missions compilation failed on #{com}=#{arg}")
			end
		}
		$result2 = ""
		$result.each{|x|
			Range.new(0,5).each{|y|
				$result2 << "#{x[y]}\x01" rescue lambda{}.call
			}
			$result2 << "\x00"
		}
		File.open("Missions.ncp","wb"){|f|f.write(Zlib::Deflate.deflate($result2,9))}
	end
	
	def self.compile_software
		$result = []
		$act_id = 0
		data = lambda{File.open('Software.ini','rb'){|f| return f.read}}.call
		data.each_line{|x|
			next if x[0] == "#" || x[0] == ";" || (x[0] == "/" && x[1] == "/")
			a = x.gsub("\n","").split("=")
			com,arg = a[0],a[1]
			case com
			when "Id"
				$act_id = arg.to_i
				$result[$act_id] = []
				$result[$act_id][0] = $act_id
			when "Name"
				$result[$act_id][1] = arg.to_s
			when "Description"
				$result[$act_id][2] = arg.to_s
			when "Level"
				$result[$act_id][3] = arg.to_i
			when "Cost"
				$result[$act_id][4] = arg.to_i
			when "CompatibleSystems"
				$result[$act_id][5] = arg.to_s
			when "Disk"
				$result[$act_id][6] = arg.to_i
			when "RAM"
				$result[$act_id][7] = arg.to_i
			when "InstallTime"
				$result[$act_id][8] = arg.to_i
			when "File"
				$result[$act_id][9] = arg.to_s
			else
				raise("Software compilation failed on #{com}=#{arg}")
			end
		}
		$result2 = ""
		$result.each{|x|
			Range.new(0,9).each{|y|
				$result2 << "#{x[y]}\x01" rescue lambda{}.call
			}
			$result2 << "\x00"
		}
		File.open("Software.ncp","wb"){|f|f.write(Zlib::Deflate.deflate($result2,9))}
	end
	
	def self.compile_network
		$result = []
		$act_id = 0
		data = lambda{File.open('Network.ini','rb'){|f| return f.read}}.call
		data.each_line{|x|
			next if x[0] == "#" || x[0] == ";" || (x[0] == "/" && x[1] == "/")
			a = x.gsub("\n","").split("=")
			com,arg = a[0],a[1]
			case com
			when "Id"
				$act_id = arg.to_i
				$result[$act_id] = []
				$result[$act_id][0] = $act_id
			when "IP"
				$result[$act_id][1] = arg.to_s
			when "Sitename"
				$result[$act_id][2] = arg.to_s
			when "Sitecontent"
				$result[$act_id][3] = arg.to_s
			when "OpenPorts"
				$result[$act_id][4] = arg.to_s
			when "SiteProtection"
				$result[$act_id][5] = arg.to_i
			when "AdminPassword"
				$result[$act_id][6] = arg.to_s
			when "AdminHashation"
				$result[$act_id][7] = arg.to_s
			when "Hidden"
				$result[$act_id][8] = arg.to_i
			when "Torred"
				$result[$act_id][9] = arg.to_i
			else
				raise("Network compilation failed on #{com}=#{arg}")
			end
		}
		$result2 = ""
		$result.each{|x|
			Range.new(0,9).each{|y|
				$result2 << "#{x[y]}\x01" rescue lambda{}.call
			}
			$result2 << "\x00"
		}
		File.open("Network.ncp","wb"){|f|f.write(Zlib::Deflate.deflate($result2,9))}
	end
	
	def self.compile_exptable
		data = lambda{File.open('ExpTable.ini','rb'){|f| return f.read}}.call
		$exptable = []
		count = 0
		data.each_line{|x|
			next if x[0] == "#" || x[0] == ";" || (x[0] == "/" && x[1] == "/")
			x.split(",").each{|y|
				count += 1
				$exptable << y
			}
		}
		$exptable2 = []
		$total = 0
		$exptable.each{|x|
			x = x.to_i
			$total += x
			$exptable2 << $total.to_s
		}
		$result2 = "0\x00"
		$exptable2.each{|x|
			$result2 << "#{x}\x00"
		}
		File.open("ExpTable.ncp","wb"){|f|f.write(Zlib::Deflate.deflate($result2,9))}
	end
	
	def self.compile_temp_missions
		$result = []
		$act_id = 0
		data = lambda{File.open('TempMissions.ini','rb'){|f| return f.read}}.call
		data.each_line{|x|
			next if x[0] == "#" || x[0] == ";" || (x[0] == "/" && x[1] == "/")
			a = x.gsub("\n","").split("=")
			com,arg = a[0],a[1]
			case com
			when "Id"
				$act_id = arg.to_i
				$result[$act_id] = []
				$result[$act_id][0] = $act_id
			when "Name"
				$result[$act_id][1] = arg.to_s
			when "Description1"
				$result[$act_id][2] = arg.to_s
			when "Description2"
				$result[$act_id][3] = arg.to_s
			when "Level"
				$result[$act_id][4] = arg.to_i
			when "StartGet"
				$result[$act_id][5] = arg.to_i
			when "Cash"
				$result[$act_id][6] = arg.to_i
			when "Exp"
				$result[$act_id][7] = arg.to_i
			when "RewardInfo"
				$result[$act_id][8] = arg.to_s
			else
				raise("Missions compilation failed on #{com}=#{arg}")
			end
		}
		$result2 = ""
		$result.each{|x|
			Range.new(0,5).each{|y|
				$result2 << "#{x[y]}\x01" rescue lambda{}.call
			}
			$result2 << "\x00"
		}
		File.open("TempMissions.ncp","wb"){|f|f.write(Zlib::Deflate.deflate($result2,9))}
	end
	
end

HackThis.compile_computers
HackThis.compile_systems
HackThis.compile_missions
HackThis.compile_software
HackThis.compile_network
HackThis.compile_exptable
HackThis.compile_temp_missions
